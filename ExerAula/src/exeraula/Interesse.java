package exeraula;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Interesse {
    private int cod;
    private String nome,interesse;
    
    public void inserir(){
        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_INTERECE (id,usuario,interece) VALUES (id_seq.nextval,?,?)";
        System.out.println("Cadastrado "+nome+" "+interesse);
        try {

            String generatedColumns[] = {"id"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, nome);
            ps.setString(2, interesse);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            
            while (rs.next()) {
                cod = rs.getInt(1);
                System.out.println("id gerado: " + cod);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    } 

    public String getInteresse() {
        return interesse;
    }

    public void setInteresse(String interesse) {
        this.interesse = interesse;
    }
    
    
}
